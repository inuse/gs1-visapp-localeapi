<?php

namespace Gs1visapp\LocaleApi;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;

use Gs1visapp\LocaleApi\TranslationApiConsumer;
use Gs1visapp\LocaleApi\WebhookVerifier;

class WebhookControllerProvider implements ControllerProviderInterface
{
    public function connect(Application $app)
    {
        // creates a new controller based on the default route
        $controllers = $app['controllers_factory'];

        
        // Simple controller, just used to test that the
        // provider is working, basically.
        $controllers->get('/', function (Application $app) {
            return "<p>Webhook controller.</p>";
        });

        // Endpoint for update webhooks.
        $controllers->post('/update-translations/', function (Application $app) {

            $allowedEvents = [
                'translations:create',
                'translations:update',
                'translations:delete',
                'translations:deliver',
            ];

            $webhookResult = WebhookVerifier::verifyHook(
                $app['request'],
                $allowedEvents,
                $app['phraseapp.webhook_secret']
            );

            if (!(is_bool($webhookResult) && $webhookResult)) {
                $app->abort(400, $webhookResult);
            }

            // Wrapping this in a try/catch as its hard to predict what
            // can go wrong with the API call.
            try {
                $data = json_decode($app['request']->getContent(), true);
                $localeCode = $data['translation']['locale']['code'];
                // Fetch the raw locale data from PhraseApp API
                $app['translationapi']->updateLocaleFile(
                    $localeCode,
                    $app['translationapi.locales']()
                );
                return "OK";
            } catch (Exception $e) {
                $app->log(sprintf("Failed to get Locale file data: '%s'.", $e));
            }
        });

        // Endpoint for update webhooks.
        $controllers->post('/update-locales/', function (Application $app) {
            $api = $app['translationapi'];

            // List of allowed events for this endpoint:
            $allowedEvents = [
                'locales:update',
                'locales:create',
                'locales:delete',
            ];

            $webhookResult = WebhookVerifier::verifyHook(
                $app['request'],
                $allowedEvents,
                $app['phraseapp.webhook_secret']
            );
            if (!(is_bool($webhookResult) && $webhookResult)) {
                $app->abort(400, $webhookResult);
            }

            try {
                // refresh the locale file
                $validLocaleData = $api->getLocaleNameMapping();
                $api->updateLocaleCacheFile($validLocaleData);

                // Get a list of valid files added to the data:
                $validLocaleFileData = $api->createValidLocaleFileList(
                    $validLocaleData
                    );
                // Set the list of files 
                $api->generateLocaleFilesToPurge($validLocaleFileData);
                $missingLocales = $api->generateMissingLocaleFiles($validLocaleFileData);

                foreach ($missingLocales as $locale) {
                    $api->updateLocaleFile($locale['code'], $missingLocales);
                }
                $api->purgeLocaleFiles();
                return "OK";
            } catch (Exception $e) {
                $app->log(sprintf("Failed to get Locale file data: '%s'.", $e));
            }
        });

        // Endpoint for refresh webhooks.
        $controllers->post('/refresh-locales/', function (Application $app) {


            // List of allowed events for this endpoint:
            $allowedEvents = [
                'test:event',
                'uploads:processing',
                'keys:delete',
                'keys:batch_delete',
            ];

            $webhookResult = WebhookVerifier::verifyHook(
                $app['request'],
                $allowedEvents,
                $app['phraseapp.webhook_secret']
            );
            if (!(is_bool($webhookResult) && $webhookResult)) {
                $app->abort(400, $webhookResult);
            }

            try {
                $app['translationapi']->refreshAllLocaleFiles();
                return "OK";
            } catch (Exception $e) {
                $app->log(sprintf("Failed to get Locale file data: '%s'.", $e));
            }
        });

        return $controllers;
    }
}
