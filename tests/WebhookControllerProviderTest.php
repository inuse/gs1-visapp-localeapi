<?php

namespace Gs1visapp\LocaleApi;

use Symfony\Component\HttpKernel;
use Symfony\Component\Filesystem\Filesystem;
use GuzzleHttp\Client;
use GuzzleHttp\Subscriber\History;
use GuzzleHttp\Subscriber\Mock;
use GuzzleHttp\Stream\Stream;
use GuzzleHttp\Message\Response;
use GuzzleHttp\Exception\ClientException;
use Gs1visapp\LocaleApi\TranslationApiConsumer;
use Gs1visapp\LocaleApi\WebhookControllerProvider;
use Silex\WebTestCase;

class WebhookControllerProviderTest extends WebTestCase
{
    public function createApplication()
    {
        $this->app = createTestApplication();
        return $this->app;
    }

    public function generateMockResponseHandler($queue)
    {
        return new Mock($queue);
    }

    public function generateMockResponseBody($content)
    {
        return Stream::factory($content);
    }
    
    public function setUp()
    {
        parent::setUp();
        $this->cacheFile = __DIR__.'/cache/locale_cache.json';
    }

    public function tearDown()
    {
        parent::tearDown();
        $fs = new Filesystem();
        $fs->remove(__DIR__.'/cache/locales/en.php');
        $fs->remove($this->cacheFile);
    }

    public function testWebhookGetEndpoint()
    {
        $client = $this->createClient();
        $crawler = $client->request('GET', '/webhooks/');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertCount(1, $crawler->filter('p:contains("Webhook controller.")'));
    }

    public function testWebhookGetUpdateTranslationsEndpoint()
    {
        // Create a mock and queue a response.
        $body = $this->generateMockResponseBody('[{"id": "foo", "code": "en", "name": "English-UK"},{"id": "bar", "code": "sv-SE","name":"Sverige-Svenska-DRAFT"}]');
        $queue = [new Response(200, [], $body), new Response(200, [], $body)];
        $mock = $this->generateMockResponseHandler($queue);

        // Set test options for the app
        $this->app['translationapi']->setClientOptions(array(), $mock);
        $this->app['translationapi']->setLocaleFileDir(__DIR__.'/cache/locales/');


        $this->app['translationapi']->setLocaleCacheFile($this->cacheFile);

        // Create fake payload:
        $content = '{"event":"translations:update","message":"jakobron updated translation for key consumer in locale UK-English in project GS1 Visibility App\n","user":{"id":"f35cabf833bbb8d2ac84e51983cbd638","username":"jakobron"},"project":{"name":"GS1 Visibility App"},"translation":{"content":"","unverified":false,"plural_suffix":"","key":{"name":"consumer","description":null},"locale":{"name":"UK-English","code":"en"}}}';
        
        // Create the hash calculation:
        $hash = trim(base64_encode(hash_hmac('sha256', $content, 'abc', true)));

        // Create the request
        $client = $this->createClient();
        $crawler = $client->request(
            'POST',
            '/webhooks/update-translations/',
            array(),
            array(),
            array(
                'CONTENT_TYPE'=>'application/json',
                'HTTP_X-PhraseApp-Signature'=> $hash,
                'HTTP_X-PhraseApp-Event' => 'translations:update'
                ),
            $content
        );
        // Try sending the request:
        $response = $client->getResponse();

        // App should now respond with 200 as the verification checks out.
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testWebhookGetUpdateLocaleEndpoint()
    {
        // Create a mock and queue a response.
        $body = $this->generateMockResponseBody('[{"id": "foobar", "code": "en", "name": "English-UK"},{"id": "barfoo", "code": "sv-SE","name":"Sverige-Svenska-DRAFT"}]');
        $queue = [
            new Response(200, [], $body),
            new Response(200, [], $body)
        ];
        // Mock handler for requests, will need to be passed into client opts.
        $mock = $this->generateMockResponseHandler($queue);

        $this->app['translationapi']->setClientOptions(array(), $mock);

        $this->app['translationapi']->setLocaleCacheFile($this->cacheFile);

        $content = '{"event":"locales:update","message":"jakobron updated translation for key consumer in locale UK-English in project GS1 Visibility App\n","user":{"id":"f35cabf833bbb8d2ac84e51983cbd638","username":"jakobron"},"project":{"name":"GS1 Visibility App"},"translation":{"content":"","unverified":false,"plural_suffix":"","key":{"name":"consumer","description":null},"locale":{"name":"UK-English","code":"en"}}}';
        $hash = trim(base64_encode(hash_hmac('sha256', $content, 'abc', true)));
        $client = $this->createClient();
        $crawler = $client->request(
            'POST',
            '/webhooks/update-locales/',
            array(),
            array(),
            array(
                'CONTENT_TYPE'=>'application/json',
                'HTTP_X-PhraseApp-Signature'=> $hash,
                'HTTP_X-PhraseApp-Event' => 'locales:update'
                ),
            $content
        );
        $response = $client->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertFileExists($this->cacheFile);
    }
}
