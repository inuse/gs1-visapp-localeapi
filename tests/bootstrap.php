<?php

namespace Gs1visapp\LocaleApi;

require __DIR__ . '/../vendor/autoload.php';

use Silex\Application;
use Gs1visapp\LocaleApi\WebhookControllerProvider;
use Gs1visapp\LocaleApi\TranslationApiServiceProvider;

class Gs1TestApp extends Application
{
    use Application\TwigTrait;
    use Application\UrlGeneratorTrait;
    use Application\MonologTrait;
    use Application\TranslationTrait;
}

function createTestApplication()
{
    $app = new Gs1TestApp();

    date_default_timezone_set('Europe/London');

    // Required settings.
    $app['phraseapp.webhook_secret'] = 'abc';
    $app['translation.file_dir'] = __DIR__.'/cache/locales';
    $app['translation.locale_name_cache_file'] = __DIR__.'/cache/locale_cache.json';
    $app['translation.api_file_format'] = 'gettext';
    $app['translation.file_suffix'] = '.po';
    $app['translation.filename_format_string'] = '<locale_code>';
    $app['phraseapp.client_options'] = array('debug', true);
    $app['phraseapp.api_token'] = 'foo';
    $app['phraseapp.project_id'] = 'bar';
    $app['phraseapp.user_agent'] = 'Gs1 Testing (http://gs1-visapp.client.inuse.se)';

    $app->register(new \Silex\Provider\MonologServiceProvider(), array(
        'monolog.logfile' => __DIR__.'/cache/logs/development.log',
    ));
    $app->register(new TranslationApiServiceProvider());

    $app['translationapi']->setClientOptions($app['phraseapp.client_options']);

    $app->mount('/webhooks', new WebhookControllerProvider());

    $app->get('/locales', function () use ($app) {
        $locales = $app['translationapi']->getLocaleNameArray();
        $result = '';
        foreach ($locales as $locale) {
            $result = $result."\n<p>".$locale['name']."</p>";
        }
        return $result;
    });
    
    // Test-specific config.
    $app['test'] = true;
    $app['exception_handler']->disable();

    return $app;
}
