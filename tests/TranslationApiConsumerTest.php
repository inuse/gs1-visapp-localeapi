<?php

namespace Gs1visapp\LocaleApi;

use Symfony\Component\HttpKernel;
use Symfony\Component\Filesystem\Filesystem;
use GuzzleHttp\Client;
use GuzzleHttp\Subscriber\History;
use GuzzleHttp\Subscriber\Mock;
use GuzzleHttp\Stream\Stream;
use GuzzleHttp\Message\Response;
use GuzzleHttp\Exception\ClientException;
use Gs1visapp\LocaleApi\TranslationApiConsumer;

class TranslationApiConsumerTest extends \PHPUnit_Framework_TestCase
{
    
    public function setUp()
    {
        parent::setUp();
        $this->cacheFile = __DIR__.'/cache/locale_cache.json';
    }

    public function tearDown()
    {
        parent::tearDown();
        $fs = new Filesystem();
        $fs->remove(__DIR__.'/cache/locales/sv-SE.po');
        $fs->remove(__DIR__.'/cache/locales/english--en.po');
        $fs->remove(__DIR__.'/cache/locales/english--en__bar.po');
        $fs->remove(__DIR__.'/cache/locales/english.po');
        $fs->remove(__DIR__.'/cache/locales/en.po');
        $fs->remove($this->cacheFile);
    }
    
    public function testCanSetClientOptions()
    {
        $client = new TranslationApiConsumer(
            array('apiToken' => 'foo', 'projectId' => 'bar')
        );
        $client->setClientOptions(array('debug' => true));
        $this->assertTrue($client->getClientOptions()['debug']);
    }

    public function testHasPublicDefaultFileFormatProp()
    {
        $client = new TranslationApiConsumer(
            array('apiToken' => 'foo', 'projectId' => 'bar')
        );
        $this->assertEquals($client->fileFormat, 'gettext');
    }
    public function testCanSetFileFormatProp()
    {
        $client = new TranslationApiConsumer(
            array('apiToken' => 'foo', 'projectId' => 'bar', 'fileFormat' => 'yml')
        );
        $this->assertEquals($client->fileFormat, 'yml');
    }

    public function testMakeLocaleFileName()
    {
        $dir = '/foo/bar';
        $dirTrailing = '/foo/bar/';
        $formatName = '<locale_name>';
        $formatId = '<locale_id>';
        $formatCode = '<locale_code>';
        $formatNameIdCode = $formatName.'--'.$formatId.'--'.$formatCode;
        $fileSuffix = '.php';
        $fileSuffixPo = '.po';

        $data = [
            'id'=> 'foo',
            'code' => 'bar',
            'name' => 'baz'
        ];

        $this->assertEquals(
            TranslationApiConsumer::makeLocaleFileName(
                $data, $formatName, $dir, $fileSuffix
                ),
            '/foo/bar/baz.php'
            );
        $this->assertEquals(
            TranslationApiConsumer::makeLocaleFileName(
                $data, $formatId, $dir, $fileSuffix
                ),
            '/foo/bar/foo.php'
            );
        $this->assertEquals(
            TranslationApiConsumer::makeLocaleFileName(
                $data, $formatNameIdCode, $dir, $fileSuffix
                ),
            '/foo/bar/baz--foo--bar.php'
            );
        $this->assertEquals(
            TranslationApiConsumer::makeLocaleFileName(
                $data, $formatNameIdCode, $dirTrailing, $fileSuffix
                ),
            '/foo/bar/baz--foo--bar.php'
            );
        $this->assertEquals(
            TranslationApiConsumer::makeLocaleFileName(
                $data, $formatNameIdCode, $dirTrailing, $fileSuffixPo
                ),
            '/foo/bar/baz--foo--bar.po'
            );
    }


    public function testCreateValidLocaleFileList()
    {
        $consumer = new TranslationApiConsumer(
            array(
                'apiToken' => 'foo',
                'projectId' => 'bar',
                'localeFileDir' => __DIR__.'/cache/locales/',
            )
        );

        $data = [
            ['id'=> 'bar', 'code'=> 'en', 'name'=> 'english'],
            ['id'=> 'foo', 'code'=> 'fr-FR', 'name'=> 'french']
        ];
        $consumer->setLocaleCacheFile($this->cacheFile);
        $consumer->updateLocaleCacheFile($data);
        $this->assertFileExists($this->cacheFile);
        $this->assertEquals(
            [
                [
                    'id' =>'bar',
                    'code'=>'en',
                    'name'=>'english',
                    'file'=>__DIR__.'/cache/locales/en.po'
                ],
                [
                    'id'=> 'foo',
                    'code'=> 'fr-FR',
                    'name'=> 'french',
                    'file'=>__DIR__.'/cache/locales/fr-FR.po'
                ],
            ],
            $consumer->createValidLocaleFileList($data)
            );
    }

    /**
     * Test that files not in the array of valid files will be purged.
     */
    public function testGenerateLocaleFilesToPurge()
    {
        $consumer = new TranslationApiConsumer(
            array(
                'apiToken' => 'foo',
                'projectId' => 'bar',
                'localeFileDir' => __DIR__.'/cache/locales/',
            )
        );
        $consumer->setLocaleCacheFile($this->cacheFile);
        $data = [
            ['id'=> 'bar', 'code'=> 'en', 'name'=> 'english'],
            ['id'=> 'foo', 'code'=> 'sv-SE', 'name'=> 'swedish']
        ];
        $fs = new Filesystem();
        $fs->touch(__DIR__.'/cache/locales/en.po');
        $fs->touch(__DIR__.'/cache/locales/sv-SE.po');
        $this->assertFileExists(__DIR__.'/cache/locales/sv-SE.po');
        $this->assertFileExists(__DIR__.'/cache/locales/en.po');
        $consumer->updateLocaleCacheFile($data);
        $validFiles = [['id'=> 'bar', 'code'=> 'en', 'name'=> 'english', 'file'=>__DIR__.'/cache/locales/en.po']];
        // Generates private property with list of files to purge:
        $consumer->generateLocaleFilesToPurge($validFiles);
        // Getter should now report array of files to delete.
        $this->assertEquals(
            [__DIR__.'/cache/locales/sv-SE.po'],
            $consumer->getLocaleFilesToPurge()
            );
    }

    /**
     * Should be able to set list of locale files to purge.
     */
    public function testSetLocaleFilesToPurge()
    {
        $consumer = new TranslationApiConsumer(
            array(
                'apiToken' => 'foo',
                'projectId' => 'bar',
                'localeFileDir' => __DIR__.'/cache/locales/',
            )
        );
        $consumer->setLocaleFilesToPurge(['foo', 'bar']);
        $this->assertEquals(['foo', 'bar'], $consumer->getLocaleFilesToPurge());
    }

    /**
     * Should be able to purge locale files.
     */
    public function testPurgeLocaleFiles()
    {
        $consumer = new TranslationApiConsumer(
            array(
                'apiToken' => 'foo',
                'projectId' => 'bar',
                'localeFileDir' => __DIR__.'/cache/locales/',
            )
        );
        $consumer->setLocaleCacheFile($this->cacheFile);
        $fs = new Filesystem();
        $fs->touch(__DIR__.'/cache/locales/en.po');
        $fs->touch(__DIR__.'/cache/locales/sv-SE.po');
        $fs->touch(__DIR__.'/cache/locales/fr-FR.po');
        $consumer->setLocaleFilesToPurge(
            [
                __DIR__.'/cache/locales/sv-SE.po',
                __DIR__.'/cache/locales/fr-FR.po'
                ]
            );
        $consumer->purgeLocaleFiles();
        $this->assertFileExists(__DIR__.'/cache/locales/en.po');
        $this->assertFileNotExists(__DIR__.'/cache/locales/sv-SE.po');
        $this->assertFileNotExists(__DIR__.'/cache/locales/fr-FR.po');
        $this->assertEquals($consumer->getLocaleFilesToPurge(), []);
    }
    /**
     * Should be able to get a list of locale files missing on disk.
     */
    public function testGenerateMissingLocaleFiles()
    {
        $consumer = new TranslationApiConsumer(
            array(
                'apiToken' => 'foo',
                'projectId' => 'bar',
                'localeFileDir' => __DIR__.'/cache/locales/',
            )
        );
        $consumer->setLocaleCacheFile($this->cacheFile);
        $fs = new Filesystem();
        $fs->touch(__DIR__.'/cache/locales/en.po');
        $fs->touch(__DIR__.'/cache/locales/sv-SE.po');

        $data = [
            ['id'=> 'bar', 'code'=> 'en', 'name'=> 'english', 'file'=>__DIR__.'/cache/locales/en.po'],
            ['id'=> 'foo', 'code'=> 'fr-FR', 'name'=> 'french', 'file'=>__DIR__.'/cache/locales/fr-FR.po']
        ];
        $consumer->updateLocaleCacheFile($data);
        $result = $consumer->generateMissingLocaleFiles($data);
        $this->assertEquals(
            $result,
            [['id'=> 'foo', 'code'=> 'fr-FR', 'name'=> 'french', 'file'=>__DIR__.'/cache/locales/fr-FR.po']]
            );
    }

    public function testGetsFullLocaleUrl()
    {
        $body = Stream::factory('foo');
        $history = new History();
        $mock = new Mock([
            new Response(200, [], $body),
        ]);
        $consumer = new TranslationApiConsumer(
            array('apiToken' => 'foo', 'projectId' => 'bar')
        );
        $consumer->setClientOptions(array(), $mock, $history);
        $result = $consumer->getLocaleFile('en', [['id'=> 'bar', 'code'=> 'en', 'name'=> 'english']]);
        $this->assertEquals('https://api.phraseapp.com/api/v2/projects/bar/locales/bar/download?file_format=gettext', $history->getLastRequest()->getUrl());
    }

    public function testGetsFullLocaleUrlHasAuthorizationToken()
    {
        $body = Stream::factory('foo');
        $history = new History();
        $mock = new Mock([
            new Response(200, [], $body),
        ]);
        $consumer = new TranslationApiConsumer(
            array('apiToken' => 'foo', 'projectId' => 'bar')
        );
        $consumer->setClientOptions(array(), $mock, $history);
        $result = $consumer->getLocaleFile('en', [['id'=> 'bar', 'code'=> 'en', 'name'=> 'english']]);
        $this->assertEquals('token foo', $history->getLastRequest()->getHeader('Authorization'));
    }

    public function testGetsFullLocaleInfoUrl()
    {
        $body = Stream::factory('foo');
        $history = new History();
        $mock = new Mock([
            new Response(200, [], $body),
        ]);
        $consumer = new TranslationApiConsumer(
            array('apiToken' => 'foo', 'projectId' => 'bar')
        );
        $consumer->setClientOptions(array(), $mock, $history);
        $result = $consumer->getAllLocaleInfo();
        $this->assertEquals('https://api.phraseapp.com/api/v2/projects/bar/locales/?per_page=100', $history->getLastRequest()->getUrl());
    }

    public function testGetsLocaleFile()
    {

        // Create a mock and queue a response.
        $body = Stream::factory('foo');

        $history = new History();
        $mock = new Mock([
            new Response(200, [], $body),
            new Response(200, [], $body),
        ]);

        $consumer = new TranslationApiConsumer(
            array('apiToken' => 'foo', 'projectId' => 'bar')
        );
        $consumer->setClientOptions(array(), $mock, $history);

        $result = $consumer->getLocaleFile('en', [['id'=> 'bar', 'code'=> 'en', 'name'=> 'english']]);
        $this->assertInstanceOf('GuzzleHttp\Message\Response', $result);
        $this->assertEquals(200, $result->getStatusCode());
        
        $this->assertEquals('foo', $result->getBody());
    }



    public function testUpdateLocaleFile()
    {
        // Create a mock and queue a response.
        $body = Stream::factory('foo');

        $history = new History();
        $mock = new Mock([
            new Response(200, [], $body),
            new Response(200, [], $body),
        ]);

        $consumer = new TranslationApiConsumer(
            array(
                'apiToken' => 'foo',
                'projectId' => 'bar',
                'localeFileDir' => __DIR__.'/cache/locales/',
            )
        );
        $consumer->setLocaleFileDir(__DIR__.'/cache/locales/');
        $consumer->setClientOptions(array(), $mock, $history);
        $result = $consumer->updateLocaleFile('en', [['id'=> 'bar', 'code'=> 'en', 'name'=> 'english']]);
        $this->assertFileExists(__DIR__.'/cache/locales/en.po');
        $this->assertFileEquals(__DIR__.'/cache/locales/en.po', __DIR__.'/cache/locales/en_test.php');
    }

    public function testUpdateLocaleFilePassData()
    {
        // Create a mock and queue a response.
        $body = Stream::factory('foo');

        $history = new History();
        $mock = new Mock([
            new Response(200, [], $body),
            new Response(200, [], $body),
        ]);

        $consumer = new TranslationApiConsumer(
            array(
                'apiToken' => 'foo',
                'projectId' => 'bar',
                'localeFileDir' => __DIR__.'/cache/locales/',
            )
        );
        $consumer->setClientOptions(array(), $mock, $history);
        $contents = 'Hello contents';
        $result = $consumer->updateLocaleFile('en', [['id'=> 'bar', 'code'=> 'en', 'name'=> 'english']], $contents);
        $this->assertFileExists(__DIR__.'/cache/locales/en.po');
        $this->assertEquals(file_get_contents(__DIR__.'/cache/locales/en.po'), $contents);
    }
    
    // public function testLocaleListResult() {
    //     $client = $this->createClient();
    //     $crawler = $client->request(
    //         'GET',
    //         '/locales');
    //     $response = $client->getResponse();
    //     $this->assertEquals(200, $response->getStatusCode());
    //     $this->assertContains('English', $response->getContent());
    //     $this->assertNotContains('Svenska', $response->getContent());
    // }

    public function testGetsAllLocaleInfoHasAuthorizationToken()
    {
        $body = Stream::factory('foo');
        $history = new History();
        $mock = new Mock([
            new Response(200, [], $body),
        ]);
        $consumer = new TranslationApiConsumer(
            array('apiToken' => 'foo', 'projectId' => 'bar')
        );
        $consumer->setClientOptions(array(), $mock, $history);
        $result = $consumer->getAllLocaleInfo();
        $this->assertEquals('token foo', $history->getLastRequest()->getHeader('Authorization'));
    }

    public function testGetAllLocaleInfo()
    {
        // Create a mock and queue a response.
        $body = Stream::factory('[{"name": "foo bar", "code": "abc", "baz": "bork"}]');
        $history = new History();
        $mock = new Mock([
            new Response(200, [], $body),
        ]);

        $consumer = new TranslationApiConsumer(
            array('apiToken' => 'foo', 'projectId' => 'bar')
        );
        $consumer->setClientOptions(array(), $mock, $history);
        
        $result = $consumer->getAllLocaleInfo();
        $this->assertEquals(200, $result->getStatusCode());
        $this->assertEquals('[{"name": "foo bar", "code": "abc", "baz": "bork"}]', $result->getBody());
    }

    public function testGetLocaleNameMapping()
    {
        // Create a mock and queue a response.
        $body = Stream::factory('[{"id": "cdef", "name": "foo bar", "code": "abc", "baz": "bork"}]');
        $history = new History();
        $mock = new Mock([
            new Response(200, [], $body),
        ]);

        $consumer = new TranslationApiConsumer(
            array('apiToken' => 'foo', 'projectId' => 'bar')
        );
        $consumer->setClientOptions(array(), $mock, $history);
        
        $result = $consumer->getLocaleNameMapping();
        $this->assertEquals([['id'=>'cdef', 'code'=>'abc', 'name'=>'foo bar']], $result);
    }
    public function testUpdateLocaleCacheFileWithData()
    {
        $consumer = new TranslationApiConsumer(
            array(
                'apiToken' => 'foo',
                'projectId' => 'bar',
                'localeFileDir' => __DIR__.'/cache/locales/',
            )
        );
        $consumer->setLocaleCacheFile($this->cacheFile);
        $data = [['id'=> 'bar', 'code'=> 'en', 'name'=> 'english']];
        $this->assertFileNotExists($this->cacheFile);
        $consumer->updateLocaleCacheFile($data);
        $this->assertFileExists($this->cacheFile);
        $this->assertEquals('[{"id":"bar","code":"en","name":"english"}]', file_get_contents($this->cacheFile));
    }


    public function testUpdateLocaleCacheFile()
    {
        // Create a mock and queue a response.
        $body = Stream::factory('[{"id": "cdef", "name": "foo bar", "code": "abc", "baz": "bork"}]');
        $updatedBody = Stream::factory('[{"id": "updated", "name": "foo bar", "code": "abc", "baz": "bork"}]');
        $history = new History();
        $mock = new Mock([
            new Response(200, [], $body),
            new Response(200, [], $updatedBody),
        ]);

        // Instantiate a new API consumer
        $consumer = new TranslationApiConsumer(
            array(
                'apiToken' => 'foo',
                'projectId' => 'bar',
                'localeFileDir' => __DIR__.'/cache/locales/',
            )
        );
        // Set options to mock response and track history.
        $consumer->setClientOptions(array(), $mock, $history);
        $consumer->setLocaleCacheFile($this->cacheFile);

        // By now, there should be no cache file.
        $this->assertFileNotExists($this->cacheFile);

        // Update the cache file (from mock response):
        $consumer->updateLocaleCacheFile($consumer->getLocaleNameMapping());
        // Now, the cache file should exist.
        $this->assertFileExists($this->cacheFile);
        // Update again, now getting the 2nd response from queue:
        $consumer->updateLocaleCacheFile($consumer->getLocaleNameMapping());
        $this->assertContains('updated', file_get_contents($this->cacheFile));
    }

    public function testGetAllLocaleIds()
    {
        // Create a mock and queue a response.
        $localeText = <<<'EOT'
[
    {"id": "cdef", "name": "English", "code": "en"},
    {"id": "abcd", "name": "Swedish", "code": "sv-SE"}
]
EOT;
        $locales = Stream::factory($localeText);
        $en = Stream::factory('English');
        $sv = Stream::factory('Swedish');

        $history = new History();
        $mock = new Mock([
            new Response(200, [], $locales),
            new Response(200, [], $en),
            new Response(200, [], $sv),
        ]);

        // Instantiate a new API consumer
        $consumer = new TranslationApiConsumer(
            array(
                'apiToken' => 'foo',
                'projectId' => 'bar',
                'localeFileDir' => __DIR__.'/cache/locales/',
            )
        );
        // Set options to mock response and track history.
        $consumer->setClientOptions(array(), $mock, $history);
        $consumer->setLocaleCacheFile($this->cacheFile);

        $consumer->updateAllLocaleFiles();
        $this->assertFileExists(__DIR__.'/cache/locales/en.po');
        $this->assertFileExists(__DIR__.'/cache/locales/sv-SE.po');
    }

    public function testFileFormatStringLocaleCodeNoParameter()
    {
        // Create a mock and queue a response.
        $body = Stream::factory('foo');

        $history = new History();
        $mock = new Mock([
            new Response(200, [], $body),
            new Response(200, [], $body),
        ]);

        $consumer = new TranslationApiConsumer(
            array(
                'apiToken' => 'foo',
                'projectId' => 'bar',
                'localeFileDir' => __DIR__.'/cache/locales/',
            )
        );
        $consumer->setClientOptions(array(), $mock, $history);
        $contents = 'Hello contents';
        $result = $consumer->updateLocaleFile('en', [['id'=> 'bar', 'code'=> 'en', 'name'=> 'english']], $contents);
        $this->assertFileExists(__DIR__.'/cache/locales/en.po');
    }
    public function testFileFormatStringLocaleName()
    {
        // Create a mock and queue a response.
        $body = Stream::factory('foo');

        $history = new History();
        $mock = new Mock([
            new Response(200, [], $body),
            new Response(200, [], $body),
        ]);

        $consumer = new TranslationApiConsumer(
            array(
                'apiToken' => 'foo',
                'projectId' => 'bar',
                'fileFormatString' => '<locale_name>',
                'localeFileDir' => __DIR__.'/cache/locales/',
            )
        );
        $consumer->setClientOptions(array(), $mock, $history);
        $contents = 'Hello contents';
        $result = $consumer->updateLocaleFile('en', [['id'=> 'bar', 'code'=> 'en', 'name'=> 'english']], $contents);
        $this->assertFileExists(__DIR__.'/cache/locales/english.po');
    }

    public function testFileFormatStringLocaleIdFormatted()
    {
        // Create a mock and queue a response.
        $body = Stream::factory('foo');

        $history = new History();
        $mock = new Mock([
            new Response(200, [], $body),
            new Response(200, [], $body),
        ]);

        $consumer = new TranslationApiConsumer(
            array(
                'apiToken' => 'foo',
                'projectId' => 'bar',
                'fileFormatString' => '<locale_name>--<locale_code>',
                'localeFileDir' => __DIR__.'/cache/locales/',
            )
        );
        $consumer->setClientOptions(array(), $mock, $history);
        $contents = 'Hello contents';
        $result = $consumer->updateLocaleFile('en', [['id'=> 'bar', 'code'=> 'en', 'name'=> 'english']], $contents);
        $this->assertFileExists(__DIR__.'/cache/locales/english--en.po');
    }
    public function testFileFormatStringLocaleNameFormatted()
    {
        // Create a mock and queue a response.
        $body = Stream::factory('foo');

        $history = new History();
        $mock = new Mock([
            new Response(200, [], $body),
            new Response(200, [], $body),
        ]);

        $consumer = new TranslationApiConsumer(
            array(
                'apiToken' => 'foo',
                'projectId' => 'bar',
                'fileFormatString' => '<locale_name>--<locale_code>__<locale_id>',
                'localeFileDir' => __DIR__.'/cache/locales/',
            )
        );
        $consumer->setClientOptions(array(), $mock, $history);
        $contents = 'Hello contents';
        $result = $consumer->updateLocaleFile('en', [['id'=> 'bar', 'code'=> 'en', 'name'=> 'english']], $contents);
        $this->assertFileExists(__DIR__.'/cache/locales/english--en__bar.po');
    }
}
