<?php

namespace Gs1visapp\LocaleApi;

use GuzzleHttp\Client;
use Webmozart\PathUtil\Path;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Finder\Finder;

class TranslationApiConsumer
{
    public $clientOptions;

    public $apiToken;

    public $projectId;

    public $fileSuffix;

    public $fileFormat;

    public $fileFormatString;

    public $localeFileDir;

    public $localeCacheFile;

    private $localeFilesToPurge;

    public $apiUrlBase;

    public $apiVersion;

    public function __construct($o = array())
    {
        $this->localeFilesToPurge = [];
        $this->apiToken = array_key_exists('apiToken', $o)? $o['apiToken']: '';
        $this->apiUrlBase = array_key_exists('apiUrlBase', $o)? $o['apiUrlBase']: 'https://api.phraseapp.com/api/';
        $this->projectId = array_key_exists('projectId', $o)? $o['projectId']: '';
        $this->apiVersion = array_key_exists('apiVersion', $o)? $o['apiVersion']: 'v2';
        $this->fileFormat = array_key_exists('fileFormat', $o)? $o['fileFormat']: 'gettext';
        $this->fileFormatString = array_key_exists('fileFormatString', $o)? $o['fileFormatString']: '<locale_code>';
        $this->fileSuffix = array_key_exists('fileSuffix', $o)? $o['fileSuffix']: '.po';
        $this->localeFileDir = array_key_exists('localeFileDir', $o)? $o['localeFileDir']: __DIR__.'/../../../cache/locales/';
        $this->localeCacheFile = array_key_exists('localeCacheFile', $o)? $o['localeCacheFile']: __DIR__.'/../../../cache/locale_cache.json';

        $this->clientOptions = array(
                            'base_url' => $this->apiUrlBase,
                            'defaults' => [
                                'headers'=> [
                                    'Authorization' => "token $this->apiToken",
                                    'User-Agent' => "GS1 Locale Api Test",
                                ],
                            ]
                        );
    }
    /**
     * Get the cached locale data, if available – returns false otherwise.
     */
    public function getLocaleCacheFileContents()
    {
        $fs = new Filesystem();
        $cacheFile = $this->localeCacheFile;

        if ($cacheFile && $fs->exists($cacheFile)) {
            return file_get_contents($cacheFile);
        }
        return false;
    }
    /**
     * Gets metadata array from cachefile, and updates cache file from
     * API if no file is present.
     */
    public function getAllLocales()
    {
        $cacheFile = $this->localeCacheFile;
        $cached = $this->getLocaleCacheFileContents($cacheFile);
        if (!$cached) {
            try {
                return $this->updateLocaleCacheFile(
                    $this->getLocaleNameMapping()
                    );
            } catch (Error $e) {
                return array();
            }
        } else {
            return json_decode($cached, true);
        }
    }
    /**
     * Get an array of locale codes, id:s and names, filtering out any
     * locales whose names match a regex. This is used to filter out
     * locales that are in a "draft" state, for now.
     *
     * @param string $ignoreRegex The regex for items to filter out.
     */
    public function getApprovedLocales($ignoreRegex = '/-DRAFT$/i')
    {
        $names = $this->getAllLocales();

        return array_filter($names, function ($item) use ($ignoreRegex) {
            if (!preg_match($ignoreRegex, $item['name'])) {
                return $item;
            }
        });
    }
    /**
     * Set which cache file name should be used.
     * @param string $fileName The name of the file.
     */
    public function setLocaleCacheFile($fileName)
    {
        $this->localeCacheFile = $fileName;
    }

    /**
     * Set locale file directory.
     * @param string $dir The directory where locale files are stored.
     */
    public function setLocaleFileDir($dir)
    {
        $this->localeFileDir = $dir;
    }


    /**
     * Extend the client options with new settings.
     */
    public function setClientOptions($newOptions, $handler = false, $historyHandler = false)
    {
        $this->clientOptions = array_merge($this->clientOptions, $newOptions);
        if ($handler) {
            $this->handler = $handler;
        }
        if ($historyHandler) {
            $this->historyHandler = $historyHandler;
        }
    }

    /**
     * Get the current client options.
     */
    public function getClientOptions()
    {
        return $this->clientOptions;
    }

    /**
     * Gets an array of assoc arrays with id, code, and name.
     */
    public function getLocaleNameMapping()
    {
        $response = $this->getAllLocaleInfo();
        $bodyString = (string) $response->getBody();
        $data = json_decode($bodyString, true);
        return array_map(function ($locale) {
            return array(
                'id' => $locale['id'],
                'code' => $locale['code'],
                'name' => $locale['name'],
            );
        }, $data);
    }

    /**
     * Gets locale data/file via the api. Returns Response object.
     */
    public function getLocaleFile($localeCode, $localeMap)
    {
        $localeId = null;

        foreach ($localeMap as $l) {
            if ($l['code'] == $localeCode) {
                $localeId = $l['id'];
            }
        }
        if (!$localeId) {
            return array();
        }

        $url = sprintf(
            "%s/projects/%s/locales/%s/download",
            $this->apiVersion,
            $this->projectId,
            $localeId
        );
        $client = new Client($this->clientOptions);
        $query = ['file_format' => $this->fileFormat];
        $result = $this->makeApiCall($url, $client, $query);

        return $result;
    }
    /**
     * Given the data (id, code, name) for a locale, make a path for it, 
     * including the filename and suffix etc.
     * @param array $localeData The id, code and name of the locale
     * @param string $fileFormatString The formatting string for the name
     * @param string $fileDir The directory for locale files.
     * @param string $fileSuffix The file suffix for locale files.
     *
     * @return string
     */
    public static function makeLocaleFileName(
        $localeData, $fileFormatString, $fileDir, $fileSuffix
        )
    {
        $fileName = $fileFormatString;
        // Simplistic string formatting, based on the currently available
        // placeholder options in the PhraseApp CLI
        if (array_key_exists('code', $localeData)) {
            $fileName = str_replace('<locale_code>', $localeData['code'], $fileName);
        }
        if (array_key_exists('id', $localeData)) {
            $fileName = str_replace('<locale_id>', $localeData['id'], $fileName);
        }
        if (array_key_exists('name', $localeData)) {
            $fileName = str_replace('<locale_name>', $localeData['name'], $fileName);
        }
        if (array_key_exists('tag', $localeData)) {
            $fileName = str_replace('<tag>', $localeData['tag'], $fileName);
        }
        $file = Path::join($fileDir, sprintf('%s%s', $fileName, $fileSuffix));

        return $file;
    }
    /**
     * Update the locale file for a specific locale(-code). Pass in the
     * locale code, the array containing all the locale metadata, and
     * optionally data to save to file.
     * If no data is passed in, the new data will be fetched from the api.
     */
    public function updateLocaleFile($locale, $localeMap, $data = false)
    {
        if ($data == false) {
            $data = $this->getLocaleFile($locale, $localeMap)->getBody();
        }
        

        // Note on below: array_column() is currently polyfilled from ramsey/array_column.
        // (see composer.json).
        // This polyfill is for 5.4 compatibility.

        // find the data for this locale code. NOTE: will need to be updated in future.
        // We will need more free-standing names, so that multiple locales with
        // same language code are possible.
        $localeDataIndex = array_search($locale, array_column($localeMap, 'code'));

        // Note: Strict type check, since array_search can return 0:
        if ($localeDataIndex !== False) {
            $localeData = $localeMap[$localeDataIndex];
        }      

        $file = $this::makeLocaleFileName(
            $localeData,
            $this->fileFormatString,
            $this->localeFileDir,
            $this->fileSuffix
            );

        $fs = new Filesystem();
        // Create the file if it doesn't exist.
        if (!$fs->exists($file)) {
            $fs->touch($file);
        }

        $fs->dumpFile($file, $data);
        return true;
    }

    /**
     * Update the cached records of locales.
     * @param data The data, probably fetched (see getLocaleNameMapping()) via API
     */
    public function updateLocaleCacheFile($data)
    {
        // Get API results:
        // $result = $this->getLocaleNameMapping();

        // Grab the filename of the cache file (json file):
        $fs = new Filesystem();
        $fileName = $this->localeCacheFile;

        // Create the cache file if it doesn't exist.
        if (!$fs->exists($fileName)) {
            $fs->touch($fileName);
        }
        // Dump new results to file.
        $fs->dumpFile($fileName, json_encode($data));
        // Return the result:
        return $data;
    }

    /**
     * Update all locale files listed in the locale cache file.
     */
    public function updateAllLocaleFiles()
    {
        $locales = $this->getAllLocales();
        foreach ($locales as $locale) {
            $this->updateLocaleFile($locale['code'], $locales);
            // Ugly but: delay next iteration by .34 seconds, to avoid making too
            // many requests to the api.
            usleep(340000);
        }
    }
    /**
     * Update the locale cache file AND then update all locale files.
     */
    public function refreshAllLocaleFiles()
    {
        // Get the current locale data:
        $validLocaleData = $this->getLocaleNameMapping();
        // Update the cache file with new data.
        $this->updateLocaleCacheFile($validLocaleData);
        // Loop through all locales and update them.
        $this->updateAllLocaleFiles();
        // Augment the data with file names:
        $validLocaleFileData = $this->createValidLocaleFileList($validLocaleData);
        // Look at the data and generate a list of files to purge:
        $this->generateLocaleFilesToPurge($validLocaleFileData);
        // Remove unused files:
        $api->purgeLocaleFiles();
    }

    /**
     * Annotate current locale metadata with file names.
     * Can be used to e.g. verify that these files exist, or delete ones who
     * don't etc.
     * 
     * @return array The metadata array + a key of 'name' for each, holding the file name.
     */
    public function createValidLocaleFileList($metadata)
    {
        return array_map(function ($val) {
            $val['file'] = $this::makeLocaleFileName(
                $val,
                $this->fileFormatString,
                $this->localeFileDir,
                $this->fileSuffix
                );
            return $val;
        }, $metadata);
    }

    /**
     * Generate list of locale files slated for purging, based on the
     * metadata argument, which should be of the structure output by
     * createValidLocaleFileList().
     *
     * @param array $metadata The array of metadata including file name. 
     *                        See createValidLocaleFileList()
     */
    public function generateLocaleFilesToPurge($metadata)
    {
        $finder = new Finder();
        $result = array();
        // Grab all translation files with the right format.
        $finder->files()->in($this->localeFileDir)->name('*'.$this->fileSuffix);
        foreach($finder as $file) {
            $found = array_search(Path::join($file->getPath(), $file->getBasename()), array_column($metadata, 'file'));
            // if not found, it is slated for purging.
            // NOTE: strict type checking equality, or the 0 index will be
            // erroneously matched as false.
            if ($found === false) {
                $result[] = Path::join($file->getPath(), $file->getBasename());
            }
        }
        $this->localeFilesToPurge = $result;
    }

    /**
     * Setter for the list of locale files slated for purging.
     *
     */
    public function setLocaleFilesToPurge($arr)
    {
        $this->localeFilesToPurge = $arr;
    }
    /**
     * Getter for the list of locale files slated for purging.
     */
    public function getLocaleFilesToPurge()
    {
        return $this->localeFilesToPurge;
    }
    /**
     * Removes locale files in the localeFilesToPurge array
     */
    public function purgeLocaleFiles() {
        $fs = new Filesystem();
        foreach($this->localeFilesToPurge as $fileName) {
            $fs->remove($fileName);
        }
        $this->localeFilesToPurge = [];
    }

    /**
     * Generate a list of any files missing on disk from metadata list.
     */
    public function generateMissingLocaleFiles($metadata)
    {
        $result = array();
        $fs = new Filesystem();
        // loop through list of valid files,
        foreach($metadata as $locale) {
            if (!$fs->exists($locale['file'])) {
                $result[] = $locale;
            }
        }
        //  if valid file is missing from existing files, download via API.
        //
        return $result;
    }

    /**
     * Sometimes, we may need to get the info for all locales, like name etc.
     * Returns the Response object.
     */
    public function getAllLocaleInfo()
    {
        $client = new Client($this->clientOptions);
        $url = sprintf("%s/projects/%s/locales/", $this->apiVersion, $this->projectId);
        // CAUTION: currently only lists 100 locales, max.
        // If there should be more than 100 locales in the future, we need to
        // figure out a pagination solution. Although, not very likely.
        $query = ['per_page' => '100'];
        return $this->makeApiCall($url, $client, $query);
    }

    private function makeApiCall($url, $client, $query = array())
    {
        // $requestOptions = array_merge_recursive($this->clientOptions, $options);
        // $client = new Client($requestOptions);

        // Kludge: if handler is set, attach it to the client.
        // Mostly needed for testing.
        if (isset($this->handler)) {
            $client->getEmitter()->attach($this->handler);
        }
        if (isset($this->historyHandler)) {
            $client->getEmitter()->attach($this->historyHandler);
        }
        try {
            $result = $client->get($url, ['query' => $query]);
        } catch (Exception $e) {
            return $e;
        }
        return $result;
    }
}
