# The Locale Api for GS1 visibility App suite.

This package defines a set of classes to extend a Silex application with support
for downloading and updating (etc) language support files.

## Technical details

This package defines a couple of ServiceProviders, meaning decoupled app
modules (Services) that another Silex application can use as a part of their own
code.

## Tests

To run tests:
```
./vendor/bin/phpunit -v
```
There is a pre-commit hook inside the `git-hooks` directory, you should install it.

## Required settings

The following parameters (set in a config file on the application itself, most likely)
are needed to use the Locale Api components.

*   `phraseapp.webhook_secret`: the webhook secret for the PhraseApp project.
    Can be found on the project settings page.
*   `translation.file_dir`: the directory where translation files are stored.
*   `translation.file_format`: the file format to use from the API.
*   `translation.file_suffix`: file format prefix to use when looking for files.
*   `translation.locale_name_cache_file`: the cache file where a current list of
    locales and their codes and names is stored.
*   `phraseapp.api_token`: the API token used for communication with PhraseApp.
*   `phraseapp.project_id`: the project ID when communicating with PhraseApp.

## Optional settings
*   `phraseapp.client_options`: defaults to empty array if not set. Used to inject
    settings into the API communication with PhraseApp (which is handled via the
    Guzzle library), for example for testing.
*   phraseapp.user_agent`: defaults to `GS1 test` or somesuch. Used when communicating
    with PhraseApp.
