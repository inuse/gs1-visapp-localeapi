<?php

namespace Gs1visapp\LocaleApi;

use Silex\Application;
use Silex\ServiceProviderInterface;
use Gs1visapp\LocaleApi\TranslationApiConsumer;

class TranslationApiServiceProvider implements ServiceProviderInterface
{
    public function register(Application $app)
    {
        // Check for the required settings:

        if (!(isset($app['phraseapp.api_token']))) {
            throw new \InvalidArgumentException('Missing parameter: phraseapp.api_token');
        }

        if (!(isset($app['phraseapp.project_id']))) {
            throw new \InvalidArgumentException('Missing parameter: phraseapp.project_id');
        }

        if (!(isset($app['translation.file_dir']))) {
            throw new \InvalidArgumentException('Missing parameter: translation.file_dir');
        }

        if (!(isset($app['translation.locale_name_cache_file']))) {
            throw new \InvalidArgumentException('Missing parameter: translation.locale_name_cache_file');
        }

        // Expose the translation API functions on the app:
        $app['translationapi'] = $app->share(function () use ($app) {

            // These options need to be set, and should be at this point.
            $options = array(
                'apiToken' => $app['phraseapp.api_token'],
                'projectId' => $app['phraseapp.project_id'],
                'localeCacheFile' => $app['translation.locale_name_cache_file'],
                'localeFileDir' => $app['translation.file_dir'],
                );

            // The rest here is optional settings.

            // This is the file format parameter that gets sent to the API:
            if (isset($app['phraseapp.api_file_format'])) {
                $options['fileFormat'] = $app['phraseapp.api_file_format'];
            }
            // This is the file suffix for the file where we save stuff:
            if (isset($app['phraseapp.file_suffix'])) {
                $options['fileSuffix'] = $app['phraseapp.file_suffix'];
            }
            // This is the format string determining the filename:
            if (isset($app['phraseapp.filename_format_string'])) {
                $options['fileFormatString'] = $app['phraseapp.filename_format_string'];
            }
            // The API version, should it change.
            if (isset($app['phraseapp.api_version'])) {
                $options['apiVersion'] = $app['phraseapp.api_version'];
            }

            return new TranslationApiConsumer($options);
        });
        // Add the locale names as a protected array, optionally callable with
        // cache file.
        $app['translationapi.locales'] = $app->protect(function () use ($app) {
            return $app['translationapi']->getAllLocales();
        });

        $app['translationapi.validlocales'] = $app->protect(function () use ($app) {
            return $app['translationapi']->getApprovedLocales();
        });
    }

    public function boot(Application $app)
    {
    }
}
