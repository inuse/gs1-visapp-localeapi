<?php

namespace Gs1visapp\LocaleApi;

class WebhookVerifier
{
    public static function verifyContentType($request, $contentType = 'application/json')
    {
        if (!$request) {
            return false;
        }

        if ($request->headers->get('Content-type') !== $contentType) {
            return false;
        }
        return $request;
    }

    public static function verifyHasEventHeader($request)
    {
        if (!$request) {
            return false;
        }
        // Check which event type:
        if (!$request->headers->has('X-PhraseApp-Event')) {
            return false;
        }
        return $request;
    }

    public static function verifyEventHeaderAccepted($request, $allowedEvents = array())
    {
        if (!$request) {
            return false;
        }
        $event = $request->headers->get('X-PhraseApp-Event');
        if (!in_array($event, $allowedEvents)) {
            return false;
        }
        return $request;
    }

    public static function verifyHasSignatureHeader($request)
    {
        if (!$request) {
            return false;
        }
        if ($request->headers->has('X-PhraseApp-Signature')) {
            return $request;
        }
    }

    public static function verifyHash($request, $secret = '')
    {
        
        if (!$request) {
            return false;
        }

        $signature = $request->headers->get('X-PhraseApp-Signature');
        $binaryHash = hash_hmac('sha256', $request->getContent(), $secret, true);
        $hash = trim(base64_encode($binaryHash));
        if ($hash !== $signature) {
            return $hash;
        }
        return true;
    }
    public static function verifyHook($request, $allowedEvents, $secret)
    {
        // $app->log(sprintf("Webhook secret: %s", $secret));
        // $app->log(sprintf("Webhook content: %s", $request->getContent()));
        // $app->log(sprintf("Bad signature: expected %s, calculated hash was %s", $signature, $hash));
        if (!self::verifyContentType($request)) {
            return 'Cannot verify content type.';
        }
        if (!self::verifyHasEventHeader($request)) {
            return 'Cannot find event header.';
        }
        if (!self::verifyEventHeaderAccepted($request, $allowedEvents)) {
            return 'Event type not allowed for this endpoint';
        }
        if (!self::verifyHasSignatureHeader($request)) {
            return 'Cannot find signature header.';
        }
        $hash = self::verifyHash($request, $secret);
        if (!(is_bool($hash) && $hash)) {
            return sprintf('Bad signature: expected %s, calculated hash was %s', $request->headers->get('X-PhraseApp-Signature'), $hash);
        }
        return true;
    }
}
